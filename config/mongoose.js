var config = require('./config'),
    mongoose = require('mongoose');
    
module.exports = function() {
    var db = mongoose.connect(config.db);
    
    require('../app/models/user.server.model');
    require('../app/models/gamification.server.model');
    require('../app/models/todo.server.model');
    require('../app/models/note.server.model');
    require('../app/models/study.server.model');
    require('../app/models/recording.server.model');
    require('../app/models/chatUser.server.model');
    require('../app/models/testmoment.server.model');
    
    return db;
}