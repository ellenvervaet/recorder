# Recorder

Deze site laat gebruikers toe om blokfluit te leren spelen. De site wordt ontwikkeld met behulp van de MEAN-stack en dient als bachelorproef voor mijn opleiding grafische en digitale media.

## Installation

1. Download het project: 'git clone https://ellenvervaet@bitbucket.org/ellenvervaet/recorder.git'
2. Installeer de node packages: 'npm install'
3. Vergeet ook bower niet: 'bower install'

4. Bower installeren.
5. mongodb installeren
Er moet ook een C:\data\db zijn, maak indien nodig zelf de folders aan.
> cd C:\Program Files\MongoDB\Server\3.4\bin 
> mongod //aanzetten
6.
> node seeder
> node server
### Digital ocean

Login: root - the hard psswrd
 
 > cd recorder
 > npm install
 > bower install
 > forever start server.js // install forever if not yet done
 
 App will run at ip:port