// grab our gulp packages
var gulp  = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    minifyCss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    sh = require('shelljs'),
    replace = require('gulp-replace-path');


// create a default task and just log a message
gulp.task('default', function() {
  return gutil.log('Gulp is running!')
});

var paths = {
  sass: ['./public/assets/scss/**/*.scss']
};

gulp.task('default', ['sass']);

gulp.task('sass', function(done) {
  gulp.src('./public/assets/scss/app.scss')
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(gulp.dest('./public/assets/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./public/assets/css/'))
    .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch('./src/**/*.js', ['scripts']);
  gulp.watch('./src/**/*.html', ['templates']);
});

/**
 * Custom Gulp Tasks.
 * 
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */

// Build the application.
gulp.task('build', ['scripts', 'templates'], build);
function build() {
    console.log(gutil.colors.blue.bgYellow.bold(' App built. '));
}

// Concatenate all scripts to `./www/js/app.js`.
gulp.task('scripts', scripts);
function scripts() {
    return gulp
        .src('./src/**/*.js')
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./public'));
}

// Copy all html files to the `./www/templates` folder.
gulp.task('templates', templates);
function templates() {
    sh.rm('-rf', './public/templates'); // Delete folder and all files within.
    return gulp
        .src('./src/**/*.html')
        .pipe(replace('/views', '')) // Verwijder views uit het pad
        .pipe(gulp.dest('./public/templates'));
}
