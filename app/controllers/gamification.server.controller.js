var mongoose = require('mongoose'),
    Level = mongoose.model('Level');

var getErrorMessage = function(err) {
    if (err.errors) {
        for (var errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].message;
        }
    } else {
        return 'Unknown server error';
    }
};

exports.list = function(req, res) {
    Level.find().sort('-name').exec(function(err, levels) {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.json(levels);
        }
    });
};

exports.read = function(req, res) {
    res.json(req.level);
};

exports.levelByName = function(req, res, next, level) {
  Level.find({
        name: level 
    },
    function(err, level) {
        if (err) {
            return next(err);
        }
        else{
            req.level = level;
            next();
        }
    });
};