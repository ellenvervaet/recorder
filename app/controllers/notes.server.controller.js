var Note = require('mongoose').model('Note');
    
//CRUD
//=================================
exports.create = function(req, res, next) {
    var note = new Note(req.body);
    note.save(function(err) {
        if (err) {
            return next(err);
        }
        else {
            res.json(note);
        }
    })
}

exports.list = function(req, res, next) {
    Note.find({}, function(err, notes) {
        if (err) {
            return next(err);
        }
        else {
            res.json(notes);
        }
    });
};
//responds with a json representation of the req.note object
exports.read = function(req, res) {
    res.json(req.note);
};

exports.noteByID = function(req, res, next, id) {
    Note.findOne({
        _id: id
    },
    function(err, note) {
        if (err) {
            return next(err);
        }
        else{
            req.note = note;
            next();
        }
    });
};

exports.notesByRank = function(req, res, next, value) {
  Note.find({
        rank: { $lt: value }
    },
    function(err, note) {
        if (err) {
            return next(err);
        }
        else{
            req.note = note;
            next();
        }
    });
};

exports.update = function(req, res, next) {
    Note.findByIdAndUpdate(req.note.id, req.body, {new: true}, function(err, note) {
        if (err) {
            return next(err);
        }
        else {
            res.json(note);
        }
    })
};

exports.delete = function(req, res, next) {
    req.note.remove(function(err) {
        if (err) {
            if (err) {
                return next(err);
            }
            else {
                res.json(req.note);
            }
        }
    })
};