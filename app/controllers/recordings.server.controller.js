var Recording = require('mongoose').model('Recording');
    
//CRUD
//=================================
exports.create = function(req, res, next) {
    var recording = new Recording(req.body);
    recording.save(function(err) {
        if (err) {
            return next(err);
        }
        else {
            res.json(recording);
        }
    })
}

exports.list = function(req, res, next) {
    Recording.find({}, function(err, recordings) {
        if (err) {
            return next(err);
        }
        else {
            res.json(recordings);
        }
    });
};
//responds with a json representation of the req.recording object
exports.read = function(req, res) {
    res.json(req.recording);
};

exports.recordingByID = function(req, res, next, id) {
    Recording.findOne({
        _id: id
    },
    function(err, recording) {
        if (err) {
            return next(err);
        }
        else{
            
            req.recording = recording;
            next();
        }
    });
};

exports.listByFeedback = function(req, res, next) {
    Recording.find({
        inFeedback: true,
        approvedFeedbacks: { $lt: 3 }
    }, function(err, recordings) {
        if (err) {
            return next(err);
        }
        else {
            res.json(recordings);
        }
    });
};

exports.recordingsByUserId = function(req, res, next, value) {
  Recording.find({
        user: value 
    },
    function(err, recording) {
        if (err) {
            return next(err);
        }
        else{
            req.recording = recording;
            next();
        }
    }).sort(
        {
            date : -1
        });
};

exports.testRecordingsByUserId = function(req, res, next, value) {
  Recording.find({
        user: value,
        testChecklist: { $ne: undefined },
        approvedFeedbacks: { $gt: 2 }
    },
    function(err, recording) {
        if (err) {
            return next(err);
        }
        else{
            req.recording = recording;
            next();
        }
    }).sort(
        {
            date : -1
        });
};

exports.update = function(req, res, next) {
    Recording.findByIdAndUpdate(req.body._id, req.body, {new: true}, function(err, recording) {
        if (err) {
            return next(err);
        }
        else {
            res.json(recording);
        }
    })
};

exports.delete = function(req, res, next) {
    req.recording.remove(function(err) {
        
        if (err) {
            return next(err);
        }
        else {
            res.json(req.recording);
        }
        
    })
};