var mongoose = require('mongoose'),
    crypto = require('crypto'), 
    Schema = mongoose.Schema;
    
var TestmomentSchema = new Schema({
    
        username: String,
        startDate: Date,
        endDate: {
            type: Date,
            expires: 0
        },
        personsAllowed: Number,
        users: [{
            username: String
        }]
});

mongoose.model('Testmoment', TestmomentSchema);