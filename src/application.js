;(function () {
    'use strict';
    
    var app = angular.module('app', [
        'ngResource', 
        'ngRoute', 
        'ngDialog',
        'app.users',
        'pubnub.angular.service',
        'app.notes',
        'app.studies',
        'app.recordings',
        'app.tests',
        'app.feedback'
        ]);
        
    angular.module('app.users', []);
    angular.module('app.notes', []);
    angular.module('app.studies', []);
    angular.module('app.recordings', []);
    angular.module('app.feedback', []);
    angular.module('app.tests', []);
    

    app.config(['$locationProvider', function($locationProvider) {
        $locationProvider.hashPrefix('!');
        }
    ]);
    
    if (window.location.hash === '#_=_')
        window.location.hash = '#!';

    angular.element(document).ready(function(){
        angular.bootstrap(document, 'app');
    });

})();
