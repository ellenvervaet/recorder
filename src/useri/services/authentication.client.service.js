;(function() {
    'use strict';
    angular.module('app.users')
    .factory('Users', Users)
    .factory('Authentication', Authentication)
    .factory('UserByName', UserByName);
    
    //Users.$inject['$resource'];
    
    function Authentication() {
        this.user = window.user;
        
        return{
            user: this.user  
        }; 
    }
    
    function Users($resource) {
        return $resource('api/users/:userId', {
            userId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
    
    function UserByName($resource) {
        return $resource('/api/usersByName/:userName', {
            userName: '@username'
        },
        {
            query: {
                method:'GET', isArray:true
            }, 
            update: {
                method: 'PUT'
            }});
    }
   
})();