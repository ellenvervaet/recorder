;(function() {
    'use strict';
    angular.module('app.users')
    .config(Routes);
    
    //Routes.$inject['$routeProvider'];
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/', {
            templateUrl: 'templates/useri/views/home.client.view.html',
            controller: 'UsersController'
        }).
        when('/profile/:profileId', {
            templateUrl: 'templates/useri/views/edit-user.client.view.html',
            controller: 'UsersController'
        });
        
    }
})();