;(function() {
    'use strict';
    
    angular.module('app.studies')
    
    .factory('Recordings', Recordings);
    //.factory('RankedStudies', RankedStudies);
    
    //Studies.$inject['$resource'];
    
    function Recordings($resource) {
        return $resource('api/recordings/:recordingId', {
            recordingId: '@_id'
        });
    }
    
    // function RankedStudies($resource) {
    //     return $resource('api/studies-rank/:rank', {
    //         rank: '@rank'
    //     });
    // }
    
})();
