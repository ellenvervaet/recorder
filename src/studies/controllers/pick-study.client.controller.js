;(function() {
    'use strict';
    
    angular.module('app.studies')
    
    .controller('PickAStudyController', PickAStudyController);
    
    //PickAStudyController.$inject['$scope', '$routeParams', '$location', 'Authentication'];
     
    function PickAStudyController($scope, $routeParams, $location, $rootScope, RankedStudies) {
        //laadt het menu
        $scope.user = $rootScope.user
        $scope.menuTemplate = "templates/partials/menu.html";
        $('.study-selected').hide();
        $scope.showDetails = function(study){
            $scope.pickedStudy = study;
            $('.study-selected').show();
            $('.study-unselected').hide();
        }

        //CRUD
        var rank = $scope.user.rank.value + 1;
        $scope.find = function() {
            $scope.studies = RankedStudies.query({
                rank: rank
            });
        };
    }
    
})();
