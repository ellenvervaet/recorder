;(function() {
    'use strict';
    
    angular.module('app.recordings')
    .factory('RecordingsUser', RecordingsUser)
    .factory('Recordings', Recordings);
    
    //Recordings.$inject['$resource'];
    
    function RecordingsUser($resource) {
        return $resource('api/recordings-user/:userId', {
            userId: '@userId'
        },             
        
        {'query':  {method:'GET', isArray:true},
        });
    }
    function Recordings($resource) {
        return $resource ('api/recordings/:recordingId', {
            recordingId: '@recordingId'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
    
})();
