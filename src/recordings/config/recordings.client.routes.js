;(function() {
    'use strict';
    angular.module('app.recordings')
    .config(Routes);
    
    function Routes($routeProvider) {
        
        $routeProvider.
        //when no recording is selected
        when('/recordings', {
            templateUrl: 'templates/recordings/views/recordings.client.view.html',
            controller: 'RecordingsController'
        })
        //when a recording is selected and we want to update,
        //we need the id in the url
        .when('/recordings/:recordingId', {
            templateUrl: 'templates/recordings/views/recordings.client.view.html',
            controller: 'RecordingsController'
        });
        
    }
})();

